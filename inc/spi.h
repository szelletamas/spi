#ifndef INC_SPI_H_
#define INC_SPI_H_

#include <sys/queue.h>
#include <stdlib.h>
#include <stdint.h>

typedef struct package_t{
	uint8_t channel;
	int16_t value;
	TAILQ_ENTRY(package_t) next;
}package_t;

typedef struct spi_list_t{
	TAILQ_HEAD(first_item,package_t) head;
	uint8_t initialized;
}spi_list_t;


/*
 * Initializing the given list.
 * If the given list is NULL or the list is already initialized then return Exit_failure.
 * */
uint8_t spi_init(spi_list_t* list);

uint8_t spi_free_package(package_t* package);

uint8_t spi_add_package(package_t* package,spi_list_t list);

package_t* spi_get_package(spi_list_t list);

uint32_t spi_is_package(spi_list_t list);
#endif /* INC_SPI_H_ */

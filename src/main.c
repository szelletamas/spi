#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <CUnit/Basic.h>
#include <spi.h>

#define TEST

#ifdef TEST
#include <test.h>
#endif


void process();
void test();

int main()
{

	#ifdef TEST
		test();
	#else
		process();
	#endif

		return 0;
}

void test() {
	CU_pSuite suite =NULL;
	if (CUE_SUCCESS!=CU_initialize_registry())
		return;

	suite = CU_add_suite("spi", 0, 0);
	if (NULL==suite) {
		CU_cleanup_registry();
		goto exit;
	}

	if ( (NULL==CU_add_test(suite, "spi init test", init_test)) ||
		 (NULL==CU_add_test(suite, "add remove test", add_remove_test)) ) {
		goto exit;
	}


	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
exit:
	CU_cleanup_registry();
}

void process(){

}



#include <test.h>
#include <spi.h>
#include <CUnit/Basic.h>
#include <stdlib.h>


/*
 * Test init function.
 * */
void init_test(){

	spi_list_t list;
	list.initialized=1;

	/* Test false cases */
	CU_ASSERT_EQUAL(spi_init(NULL),EXIT_FAILURE);
	CU_ASSERT_EQUAL(spi_init(&list),EXIT_FAILURE);

	list.initialized=0;
	/* Test true cases */
	CU_ASSERT_EQUAL(spi_init(&list),EXIT_SUCCESS);
}

void add_remove_test(){
	spi_list_t list;
	list.initialized=0;
	package_t* package =(package_t*) malloc(sizeof(package_t));
	package->channel=1;
	package->value=2;

	CU_ASSERT_EQUAL(spi_is_package(list),0);
	CU_ASSERT_PTR_NULL(spi_get_package(list)); //when trying to get a package from an un initialized list.
	CU_ASSERT_EQUAL(spi_add_package(package, list), EXIT_FAILURE); //add test when list is un init..

	CU_ASSERT_EQUAL(spi_init(&list),EXIT_SUCCESS);

	CU_ASSERT_EQUAL(spi_is_package(list),0);

	CU_ASSERT_EQUAL(spi_add_package(NULL, list), EXIT_FAILURE); //when list is initialized but it received NULL
	CU_ASSERT_EQUAL(spi_add_package(package, list),EXIT_SUCCESS);
	CU_ASSERT_EQUAL(spi_is_package(list),1);

	package_t* package_ptr=spi_get_package(list);

	CU_ASSERT_PTR_NOT_NULL(package_ptr);

	CU_ASSERT_EQUAL(spi_free_package(NULL), EXIT_FAILURE); //when free get a null ptr
	CU_ASSERT_EQUAL(spi_free_package(package_ptr),EXIT_SUCCESS);


}

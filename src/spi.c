#include <spi.h>
#include <stdlib.h>
#include <stdio.h>

#define FREE(ptr) \
		do {  \
			free(ptr); \
			ptr=NULL; \
		} while(0)

uint8_t spi_init(spi_list_t* list){

	if(list==NULL || list->initialized==1){
		return EXIT_FAILURE;
	}

	TAILQ_INIT(&list->head);
	list->initialized=1;
	return EXIT_SUCCESS;
}

uint8_t spi_free_package(package_t* package){
	if(package==NULL){
		return EXIT_FAILURE;
	}

	FREE(package);

	return EXIT_SUCCESS;

}

uint8_t spi_add_package(package_t* package,spi_list_t list){
	if(list.initialized!=1 || package==NULL){
		return EXIT_FAILURE;
	}

	TAILQ_INSERT_TAIL(&list.head,package,next);
	return EXIT_SUCCESS;
}

package_t* spi_get_package(spi_list_t list){
	if(list.initialized!=1){
		return NULL;
	}

	package_t *package = TAILQ_FIRST(&list.head);

	TAILQ_REMOVE(&list.head,package,next);
	return package;
}

uint32_t spi_is_package(spi_list_t list){
	if(list.initialized!=1){
		return 0;
	}
	int count=0;

	package_t* package=NULL;
	TAILQ_FOREACH(package,&list.head,next){
		count++;
	}

	return count;
}
